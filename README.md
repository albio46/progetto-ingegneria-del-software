# progetto ingegneria del software

1. Prova d’esame del modulo/esame di Ingegneria del Software
La prova d’esame comprende la discussione di un progetto di sviluppo di software, da
svolgere con i linguaggi e gli strumenti illustrati a lezione e nel corso delle attività di
laboratorio. I linguaggi da usare sono usualmente Python e Java, con le relative tecniche di
sviluppo delle interfacce grafiche. Per l’uso di altri linguaggi, gli studenti devono
preventivamente concordare le modalità di sviluppo del prototipo con il docente.
L’esame orale potrà poi continuare o iniziare con domande relative alle varie parti del
programma dell’insegnamento.

2. Produzione del prototipo e della documentazione di progetto
relativa
Il prototipo sarà relativo ad uno dei tre esempi dettagliati nei requisiti attraverso i documenti
presenti sulla piattaforma di e-learning di Ingegneria del Software. E’ richiesto che il
prototipo sia dotato di una opportuna interfaccia grafica. È anche possibile che gli studenti
propongano al docente progetti con prototipi in altri ambiti. Si consiglia agli studenti di
adottare la metodologia di progettazione ed implementazione ad oggetti incrementale
illustrata a lezione e di realizzare il progetto a gruppi di due o tre persone (con verifica delle
tecniche di pair-programming e pair-designing). Gruppi più numerosi sono ammessi, ma
vanno concordati prima con il docente. La progettazione e implementazione dovranno
considerare l’applicabilità dei principali pattern di progettazione e di architetture discussi a
lezione. <br>
La documentazione di progetto deve prevedere almeno (con approccio top-down):<br>
a. **Use Case principali e relative schede di specifica<br>**
b. **Sequence diagram di dettaglio per i principali Use Case<br>**
c. **Activity Diagram relativo alle modalità di interazione/operatività del software<br>**
d. **Class Diagram e Sequence diagram del software progettato<br>**
e. **Breve descrizione delle attività di test del prototipo<br>**
f. **Discussione dei principali pattern eventualmente adottati e la loro<br>
applicazione.**
