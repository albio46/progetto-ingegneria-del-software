title Login


actor Reg.User
participant SYS
actor Customer
participant "database"

Reg.User->SYS:login
activate SYS
activate Reg.User
SYS->"database":checkUser(email, pw)
activate "database"
alt credenziali corrette
SYS<<--"database":userFound(user)

Reg.User<-SYS:token
SYS-->*Customer:newCustomer(user)

note over Reg.User:Reg.User autenticato viene\n trattato ora come Customer

space
destroy Reg.User
else credenziali errate
SYS<<--"database":userFound(NULL)
deactivate "database"
Reg.User<-SYS:errore\n
deactivate SYS
space
deactivate Reg.User
end

